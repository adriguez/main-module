FROM nodejs-base:1.0

RUN git clone https://gitlab.com/adriguez/main-module.git

WORKDIR /src/main-module/app
RUN npm install

CMD [ "npm", "start" ]