const NO_PROXY = process.env.NO_PROXY = '*';
const HTTP_PROXY = process.env.HTTP_PROXY = '';
const HTTPS_PROXY = process.env.HTTPS_PROXY = '';

const { parse } = require('url');
const mainAction = require('./lib/mainAction.js');
const {send} = require('micro');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('properties/properties.file');

module.exports = async (req, res) => {

  const { query } = parse(req.url, true);

  if(Object.keys(query).length > 0){
    if(!query.action && (query.action === '' || query.action == undefined)){
      const error = new ReferenceError(
        `Falta informar una accion a realizar.`
      );
      error.statusCode = 400;
      throw error;
    }
    else{
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
      res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");

      send(res, 200, await mainAction(query.action, query));  
    }
  }

};

setInterval(async function(){
  var action = 'auto';
  var originSC = '';

  var sensores = properties.get('config.sensores');

  if(sensores !== null && sensores !== "")
  {
    var sensoresSplit = sensores.split(",");

    for(var i=0;i<sensoresSplit.length;i++){
      originSC = sensoresSplit[i];

      var query = {
        originSC: originSC
      }

      await mainAction(action, query);
    }
  }
  
}, 60000);