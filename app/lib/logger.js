const NO_PROXY = process.env.NO_PROXY = 'localhost';
const HOST_REQUEST = 'http://logging';
const ORIGIN = 'main-module';

function logger(type, logString) {

    return new Promise((resolve, reject) => {

        var request = require('request');
        urlToExec = HOST_REQUEST + `:4000/`;

        var bodyData = {
            'type': type,
            'logString': logString,
            'origin': ORIGIN
        }

        request.post({
            headers: { 'content-type': 'application/json' },
            url: urlToExec,
            body: bodyData,
            json: true
        }, function(error, response, body) {
            console.log(new Date().toUTCString() + ' ' + logString);
            return resolve(true);
        });

    });

}

module.exports = logger;