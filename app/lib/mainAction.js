var request = require('request-promise');
var parser = require('json-parser');
var sleep = require('sleep');
// logger import
const logger = require('../lib/logger.js');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('properties/properties.file');

var TEMP_EXTERIOR = "";
var TEMP_INTERIOR = "";
var HUM_INTERIOR = "";
var HUM_EXTERIOR = "";

async function mainAction(action, queryParams) {

    TEMP_EXTERIOR = "";
    TEMP_INTERIOR = "";
    HUM_INTERIOR = "";
    HUM_EXTERIOR = "";

    var urlToExec = ``;
    var autoStr = "";
    if(action == "auto") autoStr = "AUTO | ";

    var responseService = {};

    // Get Sensor data
    if (!queryParams.originSC) {

        const error = new ReferenceError(
            `Faltan parametros por informar!`
        );
        error.statusCode = 400;
        throw error;

    } else {        

        logger('info', '***** '+autoStr+'INICIAR SENSOR PRINCIPAL: ' + queryParams.originSC);

        var sensorType = properties.get('sc.' + queryParams.originSC + '.sensorType');
        var id = properties.get('sc.' + queryParams.originSC + '.sensorID');
        var name = properties.get('sc.' + queryParams.originSC + '.sensorName');
        var gpio = properties.get('sc.' + queryParams.originSC + '.sensorPin');
        var simulate = '';

        var HOST_REQUEST = properties.get('config.nameHost.sensor.' + sensorType);
        var portSensorModule = properties.get('config.portHost.sensor.' + sensorType);
        urlToExec = HOST_REQUEST + `:` + portSensorModule + `/`;

        //if (queryParams.simulate) simulate = '&simulate=1';
        //else simulate = '';

        var getParameters = '?sensorType=' + sensorType + '&idSensor=' + id + '&nameSensor=' + name + '&gpio=' + gpio + simulate;

        var options = {
            url: urlToExec + getParameters
        }

        logger('info', '***** '+autoStr+'ENVIANDO PETICION A MODULO SENSOR *****');
        logger('info', '***** '+autoStr+'URL: ' + options.url);

        // **** CALL TO SENSOR MODULE
        objectIA = await callSensorModule(options);
        // **** CALL TO IA MODULE
        responseService = await callServiceIA(objectIA, queryParams.originSC);
        // **** RETURN OBJECT TO SC MODULE
        if(responseService !== null && responseService.responseIA.action.id !== null && responseService.responseIA.temperature.label != null){

            var urlSC = properties.get('config.nameHost.scModule') + ':' + properties.get('config.portHost.scModule');
            var getParametersSC = '/?temperature=' + responseService.responseIA.temperature.label + '&action=' + responseService.responseIA.action.id;

            var accion = "";
            switch(responseService.responseIA.action.id){
                case "0": accion = "Apagar           "; break;
                case "2": accion = "Encender en FRÍO "; break;
                case "1": accion = "Encender en CALOR"; break;
            }

            await logger('info', '***** '+autoStr+'ENVIANDO ACCIÓN A REALIZAR AL MÓDULO S/C: ' + urlSC + getParametersSC + ' *****');

            var optionsSC = {
                url: urlSC + getParametersSC
            };

            var temperaturaFijar = "";
            if(responseService.responseIA.temperature.label !== "0") temperaturaFijar = responseService.responseIA.temperature.label + " ºC";
            else temperaturaFijar = "--";

            await logger('info', '╚═══════════════════════════════════════════════════════════╝');
            await logger('info', '|        · Temperatura a fijar: ' + temperaturaFijar + '                       |');
            await logger('info', '|        · Acción a realizar: ' + accion + '             |');
            await logger('info', '|  Respuesta sobre el Sistema de Climatización              |');
            await logger('info', '|                                                           |');
            await logger('info', '|        · Humedad relativa exterior: ' + HUM_EXTERIOR + '                   |');
            await logger('info', '|        · Temperatura exterior: ' + TEMP_EXTERIOR + '                       |');
            await logger('info', '|                                                           |');
            await logger('info', '|        · Humedad relativa interior: ' + HUM_INTERIOR + '                   |');
            await logger('info', '|        · Temperatura interior: ' + TEMP_INTERIOR + '                       |');
            await logger('info', '|  Factores para la respuesta de la IA:                     |');
            await logger('info', '|  Datos de: '+name+'                               |');
            if(action === "auto") await logger('info', '|  -------------------------------------------------------  |');
            if(action === "auto") await logger('info', '|  MEDICIÓN AUTOMÁTICA                                      |');
            await logger('info', '╔══════════════════════════(Climatización Inteligente v1.0)═╗');

            responseSC = await callServiceSC(optionsSC);

        }else{
            logger('error', '***** FALTAN PARÁMETROS PARA ENVIAR AL MÓDULO SC!!');
        }

    }

    return {
        response: responseService
    }

}


/**
 * Function to call Sensor module
 * @param  {Object} Object with Data to call Sensor module
 * @return {Object} Object to send IA Module
 */
function callServiceSC(options) {

    return new Promise((resolve, reject) => {

        request(options)
            .then(function(resp) {
                return resolve(true);
            })
            .catch(function(err) {
                logger('error', '***** ERROR RECIBIENDO RESPUESTA DEL MODULO SC!!! ');
                logger('error', '***** ERROR: ' + err);
                return reject(err);
            });
    });
}

/**
 * Function to call Sensor module
 * @param  {Object} Object with Data to call Sensor module
 * @return {Object} Object to send IA Module
 */
function callSensorModule(options) {

    return new Promise((resolve, reject) => {

        request(options)
            .then(function(resp) {
                var dt = new Date();
                var secs = dt.getSeconds() + (60 * (dt.getMinutes() + (60 * dt.getHours())));

                var bodyJson = parser.parse(resp);
                // Envío de temperatura interior & exterior a la IA
                if (bodyJson.temperaturaExterior !== undefined && bodyJson.temperaturaExterior !== '' && bodyJson.temperatura !== undefined && bodyJson.temperatura !== '' && bodyJson.humedadInterior !== undefined && bodyJson.humedadInterior !== '' && bodyJson.humedadExterior !== undefined && bodyJson.humedadExterior !== '') {
                    var objectIA = {
                        time: secs,
                        internalTemp: bodyJson.temperatura,
                        externalTemp: bodyJson.temperaturaExterior,
                        humidity: bodyJson.humedadInterior,
                        humidityExternal: bodyJson.humedadExterior
                    }

                    TEMP_EXTERIOR = bodyJson.temperaturaExterior;
                    TEMP_INTERIOR = bodyJson.temperatura;
                    HUM_EXTERIOR = bodyJson.humedadExterior;
                    HUM_INTERIOR = bodyJson.humedadInterior;

                    logger('info', '***** DATOS A ENVIAR AL MODULO IA: ' + JSON.stringify(objectIA));
                    return resolve(objectIA);

                }
            })
            .catch(function(err) {
                logger('error', '***** ERROR RECIBIENDO RESPUESTA DEL MODULO SENSOR!!! ');
                logger('error', '***** ERROR: ' + err);
                return reject(err);
            });
    });
}

/**
 * Function to call IA Service
 * @param  {Object} Object with IA Data to call Service
 * @return {Object} Action to send to SC
 */
function callServiceIA(objectIA, idSensor) {

    return new Promise((resolve, reject) => {
        
        var params = '/?time=' + objectIA.time + '&sensor_id=' + idSensor + '&internalTemp=' + objectIA.internalTemp + '&externalTemp=' + objectIA.externalTemp + '&humidity=' + objectIA.humidity + '&humidityExternal=' + objectIA.humidityExternal;

        var urlIA = properties.get('config.nameHost.iaModule') + ':' + properties.get('config.portHost.iaModule') + params;

        logger('info', '***** PETICIÓN ENVIADA A MÓDULO IA: ' + urlIA);

        var optionsIA = {
            uri: urlIA,
            json: true
        }

        request(optionsIA)
            .then(function(resp) {

                logger('info', '***** DATOS RECIBIDOS DE LA IA: ' + JSON.stringify(resp));
                return resolve(resp);

            }).catch(function(err) {
                logger('error', '***** ERROR RECIBIENDO RESPUESTA DEL MODULO IA!!! ');
                logger('error', '***** ERROR: ' + err);
                return reject(err);
            });
    });
}

module.exports = mainAction;